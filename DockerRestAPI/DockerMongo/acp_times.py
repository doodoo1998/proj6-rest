"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#
minspeed = {200 :15.0, 400:15.0,  600 :15.0, 1000:11.428, 1300:13.333}
maxspeed = {200: 34.0, 400: 32.0, 600:30.0 , 1000:28.0,   1300:26.0}










def time_calculator(float_hour):
  hour = int(float_hour)
  minutes = int((float_hour % 1) * 60)
  return [hour,minutes]
  


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    time = 0
    x = control_dist_km

    y = [200,400,600,1000,1300] # km list  I dont know why list(dictionary)will not return the list in order
    if (control_dist_km) > brevet_dist_km:
      control_dist_km = brevet_dist_km
    #if control dist larger than the brevet calculate like brevet
    while(x > 0):
      if (x > y[4]):
        time += (y[4] - y[3])/(maxspeed.get(y[4]))
        x = x - (y[4] - y[3])
      elif((x >  y[3]) and (x <=  y[4])):
        time += (y[3] - y[2])/(maxspeed.get(y[4]))
        x = x - (y[3] - y[2])
      elif((x >  y[2]) and ( x <=  y[3])):
        time += (y[2] - y[1])/(maxspeed.get(y[3]))
        x = x - (y[2] - y[1])
      elif((x >  y[1]) and (x <=  y[2])):
        time += (y[1] - y[0])/(maxspeed.get(y[2]))
        x = x - (y[1] - y[0])
      elif((x >  y[0]) and (x <=  y[1])):
        time += (y[0])/(maxspeed.get(y[1]))
        x = x - (y[0])
      elif(x <=  y[0]):
        time += x/(maxspeed.get(y[0]))
        x = 0
# method to add time
    hour = time_calculator(time)[0]
    minute = time_calculator(time)[1]
    #use time calculator to calculate the hour and minute
    brevet_time = arrow.get(brevet_start_time)
    open = brevet_time.shift(hours = hour, minutes = minute)

    return open.isoformat()
#return the date data


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    time = 0
    config_number = 0

    y = [200,400,600,1000,1300]
    x = control_dist_km
    if (control_dist_km) >= brevet_dist_km:
        control_dist_km = brevet_dist_km
        config_number = 1
    while(x > 0):
      if (x > y[4]):
        time += (y[4] - y[3])/(minspeed.get(y[4]))
        x = x - (y[4] - y[3])
      elif((x >  y[3]) and (x <= y[4])):
        time += (y[3] - y[2])/(minspeed.get(y[4]))
        x = x - (y[3] - y[2])
      elif((x >  y[2]) and ( x <= y[3])):
        time += (y[2] - y[1])/(minspeed.get(y[3]))
        x = x - (y[2] - y[1])
      elif((x >  y[1]) and (x <= y[2])):
        time += (y[1] - y[0])/(minspeed.get(y[2]))
        x = x - (y[1] - y[0])
      elif((x >  y[0]) and (x <=  y[1])):
        time += (y[0])/(minspeed.get(y[1]))
        x = x - (y[0])
      elif(x <= y[0]):
        time += x/(minspeed.get(y[0]))
        x = 0

    if config_number == 1:
        if control_dist_km <= 480 and control_dist_km >= 400:
            time += (20/60)
        elif control_dist_km <=  220 and control_dist_km >= 200:
            time += (10/60)


    hour = time_calculator(time)[0]
    minute = time_calculator(time)[1]
    brevet_time = arrow.get(brevet_start_time)
    close = brevet_time.shift(hours = hour, minutes = minute)

    return close.isoformat()















